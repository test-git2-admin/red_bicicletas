const { render } = require('pug');
var Bicicleta = require('../models/bicicleta.model');

/*
exports.bicicleta_list = function(req,res){
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}*/

exports.bicicleta_list = function(req,res){
    Bicicleta.find({}, function(err,bicicletas){
        res.render('bicicletas/index', {bicis: bicicletas});
    });
};

exports.bicicleta_create_get = function(req,res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req,res){
    var bici = new Bicicleta({code: req.body.id, color:req.body.modelo, modelo:req.body.color});
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}



exports.bicicleta_delete_post = function(req,res){
    Bicicleta.removeByCode(req.params.id, function(err){
        console.log(req.body.code)
        res.redirect('/bicicletas');
    });

}

exports.bicicleta_update_get = function(req,res){
    Bicicleta.findByCode(req.params.id, function(err,bici){
        console.log(bici)
        console.log(req.params.id)
        res.render('bicicletas/update',{bici});
    })
   
}

exports.bicicleta_update_post = function(req,res){
    var bici = Bicicleta.findByCode(req.params.id, function(err,bici){
        bici.code = req.body.id;
        bici.color = req.body.color;
        bici.modelo = req.body.modelo;
        bici.ubicacion = [req.body.lat, req.body.lng];
        bici.save(function(err){
            res.redirect('/bicicletas');
        });

    });

}