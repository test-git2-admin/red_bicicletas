var Usuario = require('../models/usuario.model');
var Token = require('../models/token.model');

module.exports = {
    confirmationGet : function(req,res,next){
        Token.findOne({token: req.params.token}, function(err,token){
            if(!token) return res.status(400).send({type: 'not-verified', msg: 'No encontramos un usuario con este token. quiza haya expirado y debas solcitar' + req.params.token});
            console.log("hola");
            console.log(token._userid);
            Usuario.findById(token._userId, function(err, usuario){
                if(!usuario) return res.status(400).send({msg: "No encontramos un usuario con este token" + token._userid});
                if(usuario.verificado) return res.redirect('/usuarios');
                usuario.verificado = true;
                usuario.save(function(err){
                    if(err) { return res.status(500).send({msg: err.message});}
                    res.redirect('/');
                });
            });
        });
    }
}