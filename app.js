require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require ('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');

var indexRouter = require('./routes/index.rutas');
var usersRouter = require('./routes/usuarios.rutas');
var bicicletasRouter = require('./routes/bicicletas.rutas');
var bicicletasAPIRouter = require('./routes/api/bicicletas.rutasApi');
var usuariosApiRouter = require('./routes/api/usuarios.rutasApi');
var tokenRouter = require('./routes/token.rutas');
var usuariosRouter = require('./routes/usuarios.rutas');
var authAPIRouter = require('./routes/api/auth.rutas');

const Usuario = require('./models/usuario.model');
const Token = require('./models/token.model');

let store;
if(process.env.NODE_ENV === 'development'){
  store = new session.MemoryStore;
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error', function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}


let app = express();
app.set('secretKey', 'jwt_pwd_!!223344');
app.use(session({
  cookie: {maxAge: 240*60*60*1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis_!!!!!!!****1+!*!'
}));

//mongoDB
var mongoose = require('mongoose');
const { assert } = require('console');
//var mongoDB = "mongodb://localhost/red_bicicletas";
//var mongoDB = 'mongodb+srv://admin:mj1075299716@red-bicicletas.rlr8n.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
//var mongoDB = 'mongodb://localhost/testdb';//pruebas
var mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB,{useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console,'MongoDB connecrtion error'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser())
app.use(passport.initialize());
app.use(passport.session()); 
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function(req,res){
  res.render('session/login');
});

app.post('/login', function(req, res, next){
  passport.authenticate('local',function(err, usuario, info){
    if(err) return next(err);
    if(!usuario) return res.render('session/login',{info});
    req.logIn(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req,res){
  req.logOut();
  res.redirect('/');
});

app.get('/forgotPassword', function(req,res){
  res.render('session/forgotPassword')
});

app.post('/forgotPassword', function(req,res){
  Usuario.findOne({ email: req.body.email }, function(err, usuario) {
    if(!usuario) return res.render('session/forgotPassword', {info: {message: "no existe este usuario"}});
  

    usuario.resetPassword(function(err){
      if(err) return next(err);
        console.log('session/forgotPasswordMessage')
    });

    res.render('session/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token', function(req, res, next){
  Token.findOne({token: req.params.token}, function(err, token){
    if(!token) return res.status(400).send({type: 'not-verified', msg: "no existe token"});

    Usuario.findById(token._userId, function(err, usuario){
      if(!usuario) return res.status(400).send({msg: "no existe usuario asociado a este token"});
      res.render('session/resetPassword', {errors: {}, usuario: usuario});
    });
  });
});

app.post('/resetPassword', function(req,res){
  if(req.body.pwd != req.body.confirm_pwd) {
    res.render('session/resetPassword', {errors: {confirm_password: {message: "no coinciden las claves"}}});
    return;
  }
  Usuario.findOne({ email: req.body.email }, function(err,usuario){
    usuario.password = req.body.password;
    usuario.save(function(err){
      if(err){
        res.render('session/resetPassword', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
      }else{
        res.redirect('/login')
      }
    });
  });
});

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/usuarios',usuariosRouter);
app.use('/token',tokenRouter);

app.use('/bicicletas', loggedIn, bicicletasRouter);

app.use('/api/auth',authAPIRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasAPIRouter);
app.use('/api/usuarios',usuariosApiRouter);

app.use('/privacy_policy', function(req, res){
  res.sendFile('privacy_policy.html', { root: 'public' });
});

app.use('/google5060925b8c6412e4', function(req, res){
  res.sendFile('google5060925b8c6412e4.html', { root: 'public' });
});

app.use('/googleb8cb07fa7e094d95', function(req, res){
  res.sendFile('googleb8cb07fa7e094d95.html', { root: 'public' });
});

app.get('/auth/google',
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile' ] }
));

app.get('/auth/google/callback', passport.authenticate( 'google', {
        successRedirect: '/',
        failureRedirect: '/error'
}));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if(req.user){
    next();
  } else {
    console.log('user sin loguiarse');
    res.redirect('/login');
  }
}

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if(err){
      res.json({status:"error", message: err.message, data:null});
    } else {
      req.body.userId = decoded.id;//payload, contenido de paquete
      console.log('jwt verify: ' + decoded);
      next();
    }
  });
}

module.exports = app;
