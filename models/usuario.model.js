var mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reserva.model');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 10;

const Token = require('../models/token.model');
const mailer = require('../mailer/mailer');

var Schema = mongoose.Schema;

const validateEmail = function (email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email:{
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'por favor ingrese un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password:{
        type: String,
        required: [true, "El password es obligario"]
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false
    }

});

usuarioSchema.plugin(uniqueValidator, {message: 'el {PATH} ya existe con otro usuario.'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
};
                      
usuarioSchema.methods.enviar_email_bienvenida = function (cb){
    const token1 = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token1.save(function(err){
        if(err) {return console.log(err.message);}

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            Subject: "verificacion de cuenta",
            text: "Hola,\n\n" + "Por favor, para verificar su cuenta haga click en este link: \n" + 'http://localhost:5000' + "\/token/confirmation\/" + token1.token + '.\n'
        };

        mailer.sendMail(mailOptions, function(err){
            if(err) {return console.log(err.message);}

            console.log('Se ha enviado un email de bienvenida a:' + email_destination + '.');
        });
    });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log("llego a esta funcion findOneOrCreateByGoogle");
    console.log(condition);
    console.log("name: "+ condition.displayName);
    console.log("id: "+ condition.id);    
    self.findOne({
        $or:[
            {'googleId': condition.id}, {'email': condition.emails[0].value}
    ]}, (err, result) => {
        if(result){
            callback(err, result)
        } else {
            console.log('------------- CONDITION --------------');
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = "123";//condition._json.etag;
            console.log('------------- VALUES --------------');
            console.log(values);
            self.create(values, (err, result) => {
                if(err){ console.log(err); }
                return callback(err, result)
            })
        }
    })
};


usuarioSchema.methods.resetPassword = function (cb){
    const token1 = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token1.save(function(err){
        if(err) {return console.log(err.message);}

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            Subject: "verificacion de cuenta",
            text: "Hola,\n\n" + "Por favor, para resetear el password de su cuenta haga click en este link: \n" + 'http://localhost:5000' + "\/resetPassword\/" + token1.token + '.\n'
        };

        mailer.sendMail(mailOptions, function(err){
            if(err) {return console.log(err.message);}

            console.log('Se ha enviado un email de bienvenida a:' + email_destination + '.');
        });
    });
};

module.exports = mongoose.model('Usuario',usuarioSchema);