const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario.model');
const GoogleStrategy = require('passport-google-oauth20').Strategy;

passport.use(new LocalStrategy(
    function(username, password, done){
        Usuario.findOne({email: username}, function (err, usuario){
            if(err) return done(err);
            if(!usuario) return done(null, false, {message: 'Emal no existente o incorrecto'});
            if(!usuario.validPassword(password)) return done(null, false, {message: 'Password Incorrecto'});

            return done (null,usuario);
        });
    }
));

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback"
},
    function(accessToken, refreshToken, profile, cb){
        console.log("antes del profile")
        console.log(profile);
        console.log("estrategia");
        Usuario.findOneOrCreateByGoogle(profile, function(err, user){
            return cb(err, user);
        });
    })
);

passport.serializeUser(function(user, cb){
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb){
    Usuario.findById(id, function(err, usuario){
        cb(err,usuario);
    });
});

module.exports = passport;