var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta.model');


describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open',function(){
            console.log("we are connected to test databse");
            done();
        });
    });


    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err,success){
            if(err) console.log(err);
            console.log("hecho after conexion cerrada")
            console.log("-------------------------------------------------")
            mongoose.connection.close();
            done();
        });
    });

    //1) para mi esta es la prueba que tiene error porque no tiene done y no avisa cuando termina
    describe('Bicicleta.createInstance',() => {
        it('crea una instancia de Bicicielta', () => {
            var bici = Bicicleta.createInstance(1,"verde","urbana",[-34.5,-54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
            console.log("hecho 1")
        });
    });

    //2)cuando iba a hasta aqui no consegui que me sirviera la prueba me arrojo un error  es como que no recibera el done del callback y por lo tanto no supiera 
    //cuando termina la ejecucion y se interrumpiera despues de 5 segundos
    // si lo pruebo sin el anterior describe, funciona es como que no termina correctamente el afterEach
    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                console.log("hecho 2")
                done();
            });
        });
    });
    
    //3)
    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, function(err,newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err,bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    console.log("hecho 3")
                    done();
                });
            });
        });
    });

    //4)
    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "rojo", modelo: "urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(err,targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            console.log("hecho 4")
                            done();
                        });
                    });
                });
            });
        });
    });

    //5
    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "rojo", modelo: "urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.removeByCode(1, function(err,targetBici){
                            expect(targetBici.deletedCount).toBe(1);
                            Bicicleta.allBicis(function(err,bicis){
                                expect(bicis.length).toBe(1); 
                                console.log("hecho 5")
                                done();
                            })
                        });
                    });
                });
            });
        });
    });

});



/*
beforeEach(() => {Bicicleta.allBicis=[];});

describe('Bicicleta.allBicis',() =>{
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add',() =>{
    it('agregamos una', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'Rojo','urbana',[6.1802739,-75.5795773]);
        Bicicleta.add(a)
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.FindById',() =>{
    it('debe devolver la bici con id 1', () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1,"verde","urbana");
        var aBici2 = new Bicicleta(2,"rojo","montaña");
        Bicicleta.add(aBici)
        Bicicleta.add(aBici2)

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(aBici.id);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo)
    });
});*/