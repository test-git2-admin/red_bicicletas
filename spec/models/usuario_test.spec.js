var mongoose = require("mongoose");
var Bicicleta = require('../../models/bicicleta.model');
var Usuario = require('../../models/usuario.model');
var Reserva = require('../../models/reserva.model');

describe("testing Usuario", function (){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');

            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err,success){
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err,success){
                if(err) console.log(err);
                Reserva.deleteMany({}, function(err, success){
                    if(err) console.log(err);

                    console.log("hecho after conexion cerrada")
                    console.log("-------------------------------------------------")
                    mongoose.connection.close();         
                    done();
                })
            });
        });
    });

    describe('Cuando un usuario reserva una bici', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Miguel'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color:"verde", modelo:"urbana", lat: -34, lng:-54});
            bicicleta.save();

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate() + 1);
            usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });

});