var express = require('express');
var router = express.Router();
var tokenController = require('../Controllers/token.controller');

router.get ('/confirmation/:token', tokenController.confirmationGet);

module.exports = router;