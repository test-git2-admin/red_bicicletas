var express = require('express');
var router = express.Router();
//const usuariosController = require('../controllers/usuarios.controller')
const usuariosController = require('../Controllers/usuarios.controller')


/* GET users listing. */
//esto estaba aqui pero no recuerdo porque
/*router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});*/

router.get('/', usuariosController.list);
router.get('/create', usuariosController.create_get);
router.post('/create', usuariosController.create);
router.get('/:id/update', usuariosController.update_get);
router.post('/:id/update', usuariosController.update);
router.post('/:id/delete', usuariosController.delete);

module.exports = router;
